﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using WebMVCApplication.Models;
using System.Data.Entity.Infrastructure;

namespace WebMVCApplication.Context
{
    public class MVCWebHiFiApplicationDatabaseEntities : DbContext
    {
        public MVCWebHiFiApplicationDatabaseEntities()
            : base("name=MVCWebHiFiApplicationDatabaseEntities")
        {
        }

      /*  protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }*/

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<TextEditor> TextEditors { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
    }
}