﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebMVCApplication.Models
{
    public class TextEditor
    {
        [Key]
        public Int32 EditorID { get; set; } = 0;
        [Display(Name = "Title")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Title requared")]
        public string Title { get; set; } = " ";
        [System.ComponentModel.DataAnnotations.Display(Name = "Text")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Text requared")]
        public string Text { get; set; } = " ";
        public string Message { get; set; } = " ";
        public byte[] Picture { get; set; }
        public int CategoryID { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }

        public virtual Category Category { get; set; }
    }
}