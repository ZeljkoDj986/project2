﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using WebMVCApplication.Models;
using WebMVCApplication.Controllers;

namespace WebMVCApplication.BL
{
    public class TextEditorBL
    {
        // SqlConnection sqlCon = new SqlConnection(@"Data Source=localhost\SQLEXPRESS1;Initial Catalog=MVCWebHiFiApplicationDatabase;Integrated Security=True");
        SqlConnection sqlCon = new SqlConnection(@"Data Source=DESKTOP-UO62DTN\SQLEXPRESS1;database=MVCWebHiFiApplicationDatabase;Initial Catalog=MVCWebHiFiApplicationDatabase;Integrated Security=True");
        SqlCommand sqlCom = null;
        
        public TextEditor Save(TextEditor textEditor)
        {
            try
            {
                sqlCon.Open();                
                string sql = "INSERT INTO TextEditors (Text, Title, CategoryID, DateCreate) VALUES('" + WebUtility.HtmlDecode(textEditor.Text) + "','" + WebUtility.HtmlDecode(textEditor.Title) + "','" + textEditor.CategoryID + "','" + textEditor.DateCreate + "')";
                                
                sqlCom = new SqlCommand(sql, sqlCon);
                sqlCom.ExecuteReader();
            }
            catch (Exception ex)
            {
                textEditor = new TextEditor();
                textEditor.Message = ex.Message;
            }
            finally
            {
                sqlCom.Dispose();
                sqlCon.Close();
            }
            return textEditor;
        }

        public TextEditor Get()
        {
            TextEditor textEditor = new TextEditor();

            try
            {
                sqlCon.Open();
                string sql = "SELECT TOP(1)* FROM TextEditors ORDER BY EditorID DESC";
                sqlCom = new SqlCommand(sql, sqlCon);
                SqlDataReader reader = sqlCom.ExecuteReader();

                textEditor = new TextEditor();
                while (reader.Read())
                {
                    textEditor.EditorID = Convert.ToInt32(reader["EditorID"]);
                    textEditor.CategoryID = Convert.ToInt32(reader["CategoryID"]);                    
                    textEditor.Title = WebUtility.HtmlDecode(reader["Title"].ToString());
                    textEditor.Text = WebUtility.HtmlDecode(reader["Text"].ToString());
                    textEditor.DateCreate = Convert.ToDateTime(reader["DateCreate"]);
                }                
            }
            catch (Exception ex)
            {
                textEditor = new TextEditor();
                textEditor.Text = ex.Message;
            }
            finally
            {
                sqlCom.Dispose();
                sqlCon.Close();
            }
            return textEditor;
        }     
    }
}