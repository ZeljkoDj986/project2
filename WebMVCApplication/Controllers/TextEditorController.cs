﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebMVCApplication.BL;
using WebMVCApplication.Models;
using WebMVCApplication.Context;

namespace WebMVCApplication.Controllers
{
    [Authorize]    
    public class TextEditorController : Controller
    {
        // GET: TextEditor
        public ActionResult Index()
        {           
            return View();
        }             

        TextEditorBL textEditorBL = new TextEditorBL();       

        //Save Data Text        
        [HttpPost]
        public JsonResult Save(TextEditor textEditor)
        {
            try
            {
                textEditor = textEditorBL.Save(textEditor);
            }
            catch (Exception ex)
            {
                textEditor = new TextEditor();
                textEditor.Message = ex.Message;
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string json = serializer.Serialize(textEditor);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        //Get Data Text        
        [HttpPost]
        public JsonResult Get(TextEditor textEditor)
        {
            try
            {
                textEditor = textEditorBL.Get();
            }
            catch (Exception ex)
            {
                textEditor = new TextEditor();
                textEditor.Message = ex.Message;
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string json = serializer.Serialize(textEditor);
            return Json(json, JsonRequestBehavior.AllowGet);
        }        

        MVCWebHiFiApplicationDatabaseEntities db = new MVCWebHiFiApplicationDatabaseEntities();
        //List All Text Data
        [AllowAnonymous]
        public ActionResult ViewAllText()
        {
            return View(db.TextEditors.ToList().OrderByDescending(x => x.EditorID));
        }

        //List All Text Data for Administration
        //View, Edit, and Delite text
        [Authorize (Roles = "Admin")]
        public ActionResult ViewAllTextAdministration()
        {
            return View(db.TextEditors.ToList().OrderByDescending(x => x.EditorID));
        }

        //View Last Text Data
        //View text and check to errors
        public ActionResult ViewLastText()
        {
            TextEditor textEditor;
            try
            {
                textEditor = textEditorBL.Get();
            }
            catch (Exception ex)
            {
                textEditor = new TextEditor();
                textEditor.Message = ex.Message;
            }             
            return View(textEditor);            
        }
    
        //View Just One Text Review
        public ActionResult ViewTextReview(int id = 0)
        {            
            return View(db.TextEditors.Find(id));
        }

        //Edit Text
        //Check Text for Error
        [Authorize(Roles = "Admin")]
        [ValidateInput(false)]
        public ActionResult EditText(int id)
        {
            TextEditor textEditor = db.TextEditors.Find(id);            
            if (textEditor == null)
            {
                return HttpNotFound();
            }
            return View(textEditor);
        }

        //Save Edit Text
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditText(TextEditor textEditor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(textEditor).State = System.Data.Entity.EntityState.Modified;                        
                db.SaveChanges();
                return RedirectToAction("ViewAllTextAdministration");
            }
            return View(textEditor);
        }

        //Find Id for Delete     
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            TextEditor textEditor = db.TextEditors.Find(id);
            if (textEditor == null)
            {
                return HttpNotFound();
            }
            return View(textEditor);
        }

        //Save state after Delete
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult ConfirmDeleteReview(int id = 0)
        {
            TextEditor textEditor = db.TextEditors.Find(id);
            db.TextEditors.Remove(textEditor);
            db.SaveChanges();
            return RedirectToAction("ViewAllTextAdministration");
        }
    }
}